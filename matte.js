var GLOBAL = {};

var randInt = function(from, to) {
    var range = to-from+1;
    return Math.floor(Math.random() * range) + from;
};

var mathLoad = function() {
    var maxInt = 20;
    var sign = randInt(1, 2);
    var a, b, signStr;

    if (sign == 1) {
        a = randInt(1, maxInt);
        b = randInt(1, maxInt);
        GLOBAL.answer = a + b;
        signStr = "+"; 
    } else if (sign == 2) {
        a = randInt(1, maxInt);
        b = randInt(1, a);
        GLOBAL.answer = a - b;
        signStr = "-";
    }        
    var answerElement = document.getElementById("answer");
    answerElement.classList.remove("is-valid");
    answerElement.classList.remove("is-invalid");
    answerElement.value = "";
    document.getElementById("problem").textContent = a.toFixed() +
        " " + signStr + " " + b.toFixed() + " =";
};

var mathAnswer = function() {
    var answerElement = document.getElementById("answer");
    var givenAnswer = answerElement.value;
    if (givenAnswer.length > 0 && givenAnswer == GLOBAL.answer) {
        answerElement.classList.add("is-valid");
        answerElement.classList.remove("is-invalid");
    } else {
        answerElement.classList.remove("is-valid");
        answerElement.classList.add("is-invalid");
    }        
};
